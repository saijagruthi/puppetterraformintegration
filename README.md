PuppetTerraformIntegration

Objective
The objective of this integration is to automate the configuration management of infrastructure resources provisioned using Terraform using Puppet. 

Prerequisites:
1. AWS Account
2. GitLab account
3. Terraform 
4. Puppet installed
ssh key is required to connect remote host via ssh.It consists of public key(id_rsa.pub) and private key(id_rsa).


Terraform Configuration

Provider Configuration:

Set up the AWS provider with region.

Key Pair and Security Group:

Create an key pair to the instance.
Create a security group.

EC2 Instance Provisioning:

Launch an EC2 instance with the AMI, instance type, and security group attributex.
Establish an SSH connection to the instance using the provided private key.

Puppet Installation and Configuration:

Use remote-exec provisioner to execute commands on the aws instance created and update package repositories and download Puppet Server.
Install Puppet Server.
Start the Puppet Server service.

Puppet:Apache

Create Manifest File: Create a new manifest file named apache2.pp in the directory /etc/puppetlabs/code/environments/production/modules.

Manifest Contents: Add the following Puppet code to the apache2.pp file:


package { 'apache2':
ensure => present,
}

service { 'apache2':
ensure => running,
enable => true,
}
